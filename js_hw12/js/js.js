const Arr = document.querySelectorAll('.image-to-show')
let showImg = setInterval(imgToShow, 3000)
let counter = 0
const stopBtn = document.querySelector('.stop')
const playBtn = document.querySelector('.play')


function imgToShow(){
    Arr[counter].classList.remove('show')
    counter++
    if (counter === Arr.length){
        counter = 0
    }
    Arr[counter].classList.add('show')
}

stopBtn.addEventListener('click', ()=>{
    clearInterval(showImg)
})

playBtn.addEventListener('click', ()=>{
    showImg = setInterval(imgToShow, 3000)
})