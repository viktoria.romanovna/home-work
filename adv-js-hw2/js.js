const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

function newUl (arr) {
    const filterArr = arr.map((item, index)=>{
        try{
            if(!item.author){
                throw new SyntaxError(`Отсутствует имя автора ${index}-ой книги`);
            }
            if(!item.name){
                throw new SyntaxError(`Отсутствует название ${index}-ой книги`);
            }
            if(!item.price){
                throw new SyntaxError(`Отсутствует цена ${index}-ой книги`);
            }
        } catch (error){
            if (error.name === "SyntaxError") {
                console.log( `${error.stack}  |  Details: ${error.message}` );
            } else {
                throw error;
            }
        } finally {
            if (item.author && item.name && item.price) {
                return `<li>${item.author} "${item.name}" ${item.price}</li>`;
            }
        }
    }).filter(Boolean);
    const strHtml = `<ul>Книги: ${filterArr.join('')}</ul>`;
    document.querySelector('#root').insertAdjacentHTML("afterbegin", strHtml);
}
newUl(books);
