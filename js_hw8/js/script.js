let questionDiv = document.createElement("div");
let questionName = document.createElement('p');
let question = document.createElement('input', '',);
let infoDiv = document.createElement('div');
let infoSpan = document.createElement('span');
let closeBtn = document.createElement('button');
let bug = document.createElement('p');

questionDiv.style.display = "flex";
questionDiv.style.padding = "10px";
questionDiv.style.width = "220px"
questionDiv.style.margin ="10px";
questionDiv.style.backgroundColor = "grey"
document.body.append(questionDiv);
questionName.innerHTML = "Price";
questionName.style.paddingRight = '5px';
questionDiv.prepend(questionName);
question.id = "input-question";
question.type ="number";
question.style.border = "5px solid transparent";
question.style.outline = "none";
questionDiv.append(question);
question.addEventListener("focus", (event) =>{
    question.style.borderColor = "green";
});
question.addEventListener("blur", (event) => {
    if (question.value <= 0){
        question.style.borderColor = "red";
        bug.innerHTML = "Please enter correct price";
        bug.style.margin = "10px";
        infoDiv.remove();
        questionDiv.after(bug);
    } else {
        question.style.borderColor = 'transparent';
        infoDiv.style.width = "170px";
        infoDiv.style.border = "1px solid grey";
        infoDiv.style.margin = "10px";
        questionDiv.before(infoDiv);
        infoSpan.style.padding = "10px";
        infoSpan.innerHTML = `Текущая цена: ${question.value}`;
        infoDiv.prepend(infoSpan);
        closeBtn.style.width = "20px";
        closeBtn.style.height = "20px";
        closeBtn.innerText = "x";
        closeBtn.style.backgroundColor = "transparent";
        closeBtn.style.outline = "none";
        infoDiv.append(closeBtn);
        question.style.color = 'green';
        bug.remove();
    }
});

closeBtn.addEventListener('click', () =>{
    infoDiv.style.display = "none";
    question.value = "none";
})
