const $hiddenBtn = $('.news-btn')
const $newsHot = $('.news-hot-toggle')
const $scrollBtn = $('.scroll-btn')
const $menuList = $('.menu-list')

$hiddenBtn.on('click', ()=>{
    $newsHot.toggle('hidden')
})

$scrollBtn.click(() => {
    $('html, body').animate({scrollTop: 0}, 1000);
});

$(window).scroll(function (){
    let $windowScroll = $(window).scrollTop()
    if($windowScroll >= 380){
        $scrollBtn.removeClass('hidden-btn')
    } else {
        $scrollBtn.addClass('hidden-btn')
    }
})


$("#menu").on("click","a", function (event) {
    let id  = $(this).attr('href');
    console.log(id);
    let top = $(id).offset().top;
    $('body,html').animate({scrollTop: top}, 1500);
});