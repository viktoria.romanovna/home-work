const user1 = {
    name: "John",
    years: 30
};

const {
    name, years: age, isAdmin = false
} = user1;

const textElement = document.createElement("p")
textElement.textContent = `${name} ${age} ${isAdmin}`

document.body.append(textElement)



