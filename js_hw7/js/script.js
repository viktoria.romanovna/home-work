const createUl = function (arr, parent = document.body){
    let newArr = arr.map(item => `<li>${item}</li>`);
    let newUl = document.createElement('ul');
    newArr.forEach(item => {
        newUl.insertAdjacentHTML('beforeend', item)
    });
    parent.prepend(newUl);
}

createUl (["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);
