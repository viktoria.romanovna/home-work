import React from 'react'
import className from './Modal.css'
import Cards from "../Cards/Cards";

const Modal = (props) => {

        const {open, closeBtn, actions} = props
        if (open) {
            return (
                <div className={'popup'}>
                    <div className={'modal'}>
                        <header className={'modal-header'}>
                            <h2 className={'modal-title'}>
                                Корзина
                            </h2>
                            <button className={'close-x'} onClick={closeBtn}>
                                x
                            </button>
                        </header>
                        <div className={'modal-info'}>
                            <p className={'modal-text'}>Желаете добавить товар в корзину?</p>
                            {actions.btn()}
                        </div>
                    </div>
                </div>
            )
        }
         return ('')
}

// Modal.propTypes = {
//     open: PropTypes.bool.isRequired,
//     closeBtn: PropTypes.func.isRequired,
//     actions: PropTypes.func.isRequired
// }

export default Modal;