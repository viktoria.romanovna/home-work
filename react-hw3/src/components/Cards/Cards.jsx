import React, {useState} from 'react'
import className from './Cards.css'
import Svg from "../SVG/Svg";
import PropTypes from 'prop-types'


const Cards = (props) => {
    const {item, showModal, changeColor, state, favoriteAddLocalStorage} = props

    // const {item, showModal, changeColor, state, favoriteAddLocalStorage} = this.props
    return(
        <div className={'cards'}>
            {item.map(elem => {
                return (
                    <div className={'cards-elem'} key={elem.article}>
                        <p>{elem.name}</p>
                        <img src={`${elem.img}`} width={'300px'} height={'300px'} />
                        <div className={'def-div'} key={elem.article}>
                            <p>{elem.article}</p>
                            <p>{elem.price}</p>
                        </div>
                        <div className={'def-div'}>
                            <button onClick={(e)=>showModal(e)} id={elem.article}>Add to card</button>
                            <Svg fill={elem.favorite} changeColor={changeColor} favoriteAddLocalStorage={favoriteAddLocalStorage} state={state} id={elem.article}/>
                        </div>
                    </div>
                )
            }
            )}
        </div>
    )
}

// Cards.propTypes = {
//     item: PropTypes.array.isRequired,
//     showModal: PropTypes.func.isRequired,
//     changeColor: PropTypes.func.isRequired,
//     state: PropTypes.bool.isRequired,
//     favoriteAddLocalStorage: PropTypes.func.isRequired
// }

export default Cards;