import React, {useState, useEffect} from 'react'
import className from './Lists.css'
import Cards from "../Cards/Cards";



const List = (props) =>{

    const {items} = props

    return(
             <ul className={'lists'}>
                 {items.map(item => {
                     return (
                         <li className={'lists-elem'} key={item.article}>{item.name}</li>
                     )
                 })}
             </ul>
         )
}


export default List;