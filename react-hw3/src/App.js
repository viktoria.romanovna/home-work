import React, {useState, useEffect} from 'react'
 import className from './App.css'

import Cards from "./components/Cards/Cards";
import List from "./components/List/Liist";
import Modal from "./components/Modal/Modal";

const App = () =>{
    const [isOpen, setIsOpen] = useState(false)
    const [items, setItems] = useState([])
    const [isFavorite, setIsFavorite] = useState(false)
    const [id, setId] = useState([])


     const normalizeData = (result) =>{
         result.map(item =>{
             const saveItems = localStorage.getItem('favorites') || []
             item.favorite = saveItems.includes(item.article.toString())
             item.add = false
             return item
         })
     }

     const getComponent = () =>{
         fetch("http://localhost:3000/cards.json")
             .then(res => res.json())
             .then(
                 (result) => {
                     normalizeData(result)
                     setItems(
                         result
                     );
                 })
     }

    useEffect(()=> {
        getComponent()
    },[])

     const showModal = (e) => {
         setIsOpen(!e.isOpen)
         setId(e.target.id)
     }

     const closeModal = (e) => {
         if(e.target.className==='popup'){
             setIsOpen( false);
         }
     }

     const closeBtn = () =>{
         setIsOpen(false);
     }

     const changeColor = (id) => {
         let favoriteItem = JSON.parse(localStorage.getItem('favorites'))|| [];
         console.log(favoriteItem);
         favoriteItem = (favoriteItem.includes(id)? favoriteItem.filter(i => i !== id):favoriteItem.concat([id]))
         let favoriteItemsArr = JSON.stringify(favoriteItem)
         localStorage. setItem ('favorites', favoriteItemsArr);
         const newItems = items.map(i =>{
             if(i.article === id){
                 i.favorite = !i.favorite
                 console.log(i);
             }
             return i
         })
         setItems(newItems)
         console.log(items);
     }

     const acceptAddFavorite = (e) =>{
         const id = e.target.id
         let acceptItem = JSON.parse(localStorage.getItem('accept'))|| [];
         console.log(acceptItem);
         acceptItem = (acceptItem.includes(id)? acceptItem.filter(i => i !== id):acceptItem.concat([id]))
         let acceptItemsArr = JSON.stringify(acceptItem)
         localStorage. setItem ('accept', acceptItemsArr);
         setIsOpen (false);
     }

    console.log(items);
    return (
             <div className={'content-block'}>
                 <List items={items}/>
                 <Cards
                     item={items} showModal={showModal} id={id}
                      color={'red'} changeColor={changeColor}
                      state = {items}
                  />
                 <div onClick={(e)=>closeModal(e)}>
                     <Modal
                         open={isOpen} close={isOpen} closeBtn={closeBtn}
                         actions={{
                             btn: () => (
                                 <div className={'btn-block'}>
                                     <button className={'btn-ok'} id={id} onClick={(e) => acceptAddFavorite(e)}>OK</button>
                                     <button className={'btn-close'} onClick={closeBtn}>Close</button>
                                 </div>
                             )
                         }}
                     />
                 </div>
             </div>
          )
  }

export default App;

