const changeBtn = document.querySelector('.change-btn')
const backgroundChange = document.querySelector('.global-background')
const changeHover = document.querySelectorAll('.opacity')
const changeColorArr = document.querySelectorAll('.tour')
const changeTitleBlock = document.querySelectorAll('.title-block')
const changeLogo = document.querySelectorAll('.logo')
const changeTitleLogo = document.querySelectorAll('.orange')
const changeContacts = document.querySelectorAll('.contacts')
const changeMail = document.querySelectorAll('.mail')
const localBtn = changeBtn.classList
const changeColor = () =>{ if(
    changeBtn.classList.contains('change-btn')){
    changeBtn.classList.remove('change-btn')
    changeBtn.classList.add('change-btn-different')
} else {
    changeBtn.classList.remove('change-btn-different')
    changeBtn.classList.add('change-btn')
}
    localStorage.setItem('6',localBtn)

    if (backgroundChange.classList.contains('global-background')) {
        backgroundChange.classList.remove('global-background')
        backgroundChange.classList.add('global-background-change')
    } else {
        backgroundChange.classList.remove('global-background-change')
        backgroundChange.classList.add('global-background')
    }

    if (changeColorArr[0].classList.contains('tour')) {
        changeHover.forEach(hvr => {
            if (hvr.classList.contains('opacity')) {
                hvr.classList.remove('opacity')
                hvr.classList.add('change-hover')
            }
            else {
                hvr.classList.add('opacity')
                hvr.classList.remove('change-hover')
            }
        })
        changeTitleBlock.forEach(item =>{
            if(item.classList.contains('title-block')){
                item.classList.remove('title-block')
                item.classList.add('change-title-block')
            }
            else {
                item.classList.add('title-block')
                item.classList.remove('change-title-block')
            }
        })
    }

    if (changeLogo[0].classList.contains('logo')) {
        changeTitleLogo.forEach(item => {
            if (item.classList.contains('orange')) {
                item.classList.remove('orange')
                item.classList.add('indigo')
            }
            else {
                item.classList.remove('indigo')
                item.classList.add('orange')
            }
        })
    }

    if (changeContacts[0].classList.contains('contacts')) {
        changeMail.forEach(item => {
            if (item.classList.contains('mail')) {
                item.classList.remove('mail')
                item.classList.add('change-mail')
            }
            else {
                item.classList.remove('change-mail')
                item.classList.add('mail')
            }

        })
    }
}

const changeTheme = function () {
    if (localStorage.getItem('6', localBtn) === 'change-btn-different') {
        changeColor();
    }
}
changeTheme();

changeBtn.addEventListener('click', (e) => {
    changeColor();
})
