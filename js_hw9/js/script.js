const eventContainer = document.querySelector('.centered-content')
const eventArr = document.querySelectorAll('.tabs-title')
const contentArr = document.querySelectorAll('.hidden')

eventContainer.addEventListener('click', (e) =>{
    const cursor = e.target
    eventArr.forEach(item=>{
    if(item.classList.contains('tabs-active')){
        item.classList.remove('tabs-active')
    }
    })
    cursor.classList.add('tabs-active')

    contentArr.forEach(arr =>{
        if(cursor.getAttribute('data-name') === arr.getAttribute('data-name')){
            arr.classList.add('content-active')
         } else if(cursor.getAttribute('data-name') !== arr.getAttribute('data-name')){
            arr.classList.remove('content-active')
        }
    })
})
