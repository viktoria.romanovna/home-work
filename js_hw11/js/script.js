const clickArr = document.querySelectorAll('.btn')

window.addEventListener('keydown', (e) => {
    const keyName = e.key

    clickArr.forEach((btn) => {
        if(btn.classList.contains('btn-active')){
            btn.classList.remove('btn-active')
        }
        if (keyName.toUpperCase() === btn.textContent) {
            btn.classList.add('btn-active')
            console.log(btn.textContent);
        }
        if (keyName === "Enter") {
            clickArr[0].classList.add('btn-active')
        }
    })
})