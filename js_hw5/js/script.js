function createUser() {
    const user = {
        firstName: prompt("Enter your First Name, please!"),
        lastName: prompt("Enter your Last Name, please!"),
        birthday: prompt("Enter your birthday date in format dd.mm.yyyy, please!"),
        getAge: function(){
            let dateUTC = new Date(this.birthday + "Z");
            return Math.trunc((new Date() - dateUTC)/31536000000);
        },
        getPassword: function () {
            let dateUTC = new Date(this.birthday + "Z");
            return this.firstName[0].toLocaleUpperCase() + this.lastName.toLowerCase() + dateUTC.getFullYear();
        }
    }
    return user
}
const newUser = createUser();
console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());