const menuBtn = document.querySelector('.menu-btn')
const menuMobile = document.querySelector('.menu__nav-mobile')

menuBtn.addEventListener('click', ()=>{
    if(menuMobile.classList.contains('menu__nav-mobile')){
        menuMobile.classList.remove('menu__nav-mobile')
        menuMobile.classList.add('menu__nav-mobile-active')
    } else {
        menuMobile.classList.remove('menu__nav-mobile-active')
        menuMobile.classList.add('menu__nav-mobile')
    }
    menuBtn.classList.toggle('menu-btn-active')
})