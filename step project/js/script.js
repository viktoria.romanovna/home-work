$(".our-list").on("click", ".our-service-link", function (even) {
    $(".our-list .our-service-link").removeClass("our-link-click");
    $(this).addClass("our-link-click");
    const $servicesItem = $('.our-service-link');
    const $cursorTarget = even.target
    if ($cursorTarget === $servicesItem[0]) {
        if($('.hidden').hasClass('show')){
            $('.hidden').removeClass('show')
        }
        $('#our-list-hidden-web').addClass('show')
    }
    else if  ($cursorTarget === $servicesItem[1]) {
        if($('.hidden').hasClass('show')){
            $('.hidden').removeClass('show')
        }
        $('#our-list-hidden-graphic').addClass('show')
    }
    else if ($cursorTarget === $servicesItem[2]) {
        if($('.hidden').hasClass('show')){
            $('.hidden').removeClass('show')
        }
        $('#our-list-hidden-support').addClass('show')
    } else if ($cursorTarget === $servicesItem[3]) {
        if($('.hidden').hasClass('show')){
            $('.hidden').removeClass('show')
        }
        $('#our-list-hidden-app').addClass('show')
    }  else if ($cursorTarget === $servicesItem[4]) {
        if($('.hidden').hasClass('show')){
            $('.hidden').removeClass('show')
        }
        $('#our-list-hidden-marketing').addClass('show')
    }  else if ($cursorTarget === $servicesItem[5]) {
        if($('.hidden').hasClass('show')){
            $('.hidden').removeClass('show')
        }
        $('#our-list-hidden-seo').addClass('show')
    }
})

$(".filter-items-list").on("click", ".our-link-filter", function (even) {
    const $filterItem = $('.our-link-filter')
    const $galleryItems = $(document.querySelectorAll('.filter-items-container'));
    if(even.target === $filterItem[0]) {
        $galleryItems.removeClass('filter-items-show')
        let $galleryShow = ($("div.filter-items-container:lt(12)"));
        $galleryShow.addClass('filter-items-show');
    } else if (even.target === $filterItem[1]) {
        $galleryItems.removeClass('filter-items-show')
        const $galleryItemsArr = $galleryItems.filter($(document.querySelectorAll('.graphic-design-filter')))
        $galleryItemsArr.addClass('filter-items-show')
    } else if (even.target === $filterItem[2]) {
        $galleryItems.removeClass('filter-items-show')
        const $galleryItemsArr = $galleryItems.filter($(document.querySelectorAll('.web-design-filter')))
        $galleryItemsArr.addClass('filter-items-show')
    } else if (even.target === $filterItem[3]) {
        $galleryItems.removeClass('filter-items-show')
        const $galleryItemsArr = $galleryItems.filter($(document.querySelectorAll('.landing-page-filter')))
        $galleryItemsArr.addClass('filter-items-show')
    } else if (even.target === $filterItem[4]) {
        $galleryItems.removeClass('filter-items-show')
        const $galleryItemsArr = $galleryItems.filter($(document.querySelectorAll('.wordpress-filter')))
        $galleryItemsArr.addClass('filter-items-show')
    }
})


$(".filter-btn").on("click", function (even) {
   const $filterBlock = $(document.querySelectorAll('.filter-items-container'));
   $filterBlock.removeClass('filter-items-show');
   let $showFilter24 = ($("div.filter-items-container:lt(24)"));
   $showFilter24.addClass('filter-items-show');
});

$('.sl').slick({
    autoplay:false,
    arrows:true,
    dots:true,
    lazyLoad: 'progressive',
    responsive: [
        {
            settings: {
                dots:true,
                arrows:false,
            }
        },
        {
            settings: {
                dots:true,
                arrows:false,
            }
        }
    ]
});