import React from 'react'
import className from './Modal.css'

class Modal extends React.Component {

    render() {
        const {open1, open2, closeBtn, actions} = this.props
        console.log(open1);
        if (open1) {
            return (
                <div className={'popup'}>
                    <div className={'modal'}>
                        <header className={'modal-header'}>
                            <h2 className={'modal-title'}>
                                Modal 1
                            </h2>
                            <button className={'close-x'} onClick={closeBtn}>
                                x
                            </button>
                        </header>
                        <div className={'modal-info'}>
                            <p className={'modal-text'}>Modal 1</p>
                            {actions.btn()}
                        </div>
                    </div>
                </div>
            )
        } if (open2) {
            return (
                <div className={'popup'}>
                    <div className={'modal'}>
                        <header className={'modal-header'}>
                            <h2 className={'modal-title'}>
                                Modal 2
                            </h2>
                            <button className={'close-x'} onClick={closeBtn}>
                                x
                            </button>
                        </header>
                        <div className={'modal-info'}>
                            <p className={'modal-text'}>Modal 2</p>
                            {actions.btn()}
                        </div>
                    </div>
                </div>
            )
        }

         return ('')
    }
}

export default Modal;