import React from 'react'
import className from '../Button/Button.css'

class Button extends React.Component{

    render() {
        const {onClick, text, backgroundColor} = this.props
        console.log(onClick);
        return (
            <div className="button">
                <button className = {"btn-elem"}  style={{backgroundColor}} onClick={onClick}>{text}</button>
            </div>
        );
    }

}

export default Button;