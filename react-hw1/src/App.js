import React from 'react'
import className from './App.css'

import Button from "./components/Button/Button";
import Modal from './components/Modal/Modal'

class App extends React.Component{

    constructor() {
        super();
        this.state = {isOpen1: false, isOpen2: false}
    }

    showModal1 = () => {
        this.setState({ isOpen1: true});
    }

    showModal2 = () => {
        this.setState({ isOpen2: true});
    }

    closeModal = (e) => {
        if(e.target.className==='popup'){
            this.setState({ isOpen1: false, isOpen2: false });
        }
    }

    closeBtn = () =>{
        this.setState({ isOpen1: false, isOpen2: false });
    }


  render(){
        const {isOpen1, isOpen2} = this.state

    return(
            <div className='btn-block'>
                <Button onClick={this.showModal1} backgroundColor={'#8D81AC'} text={'Open first modal'}/>
                <Button onClick={this.showModal2} backgroundColor={'#14B9D5'} text={'Open second modal'}/>
                <div onClick={(e)=>this.closeModal(e)}>
                    <Modal open1 ={isOpen1} open2 ={isOpen2} close ={isOpen2} closeBtn={this.closeBtn}
                           xBtn = {true}
                           actions = {{btn: ()=>(
                               <div className={'btn-block'}>
                                   <button className={'btn-ok'} onClick={this.closeBtn}>OK</button>
                                   <button className={'btn-close'} onClick={this.closeBtn}>Close</button>
                               </div>
                               ) }}
                    />
                </div>
            </div>
        )
  }
}

export default App;

