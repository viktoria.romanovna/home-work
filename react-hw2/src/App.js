import React from 'react'
 import className from './App.css'

import Cards from "./components/Cards/Cards";
import List from "./components/List/Liist";
import Modal from "./components/Modal/Modal";

 class App extends React.Component {
     constructor() {
         super();
         this.state = {
             isOpen: false,
             items: [],
             isFavorite: false,
             id: []
         }
     }

     normalizeData(result){
         console.log(result);
         result.map(item =>{
             const saveItems = localStorage.getItem('favorites') || []
             console.log(saveItems);
             console.log(item.article);
             item.favorite = saveItems.includes(item.article.toString())
             item.add = false
             return item
         })
     }

     getComponent(){
         fetch("/cards.json")
             .then(res => res.json())
             .then(
                 (result) => {
                     this.normalizeData(result)
                     this.setState({
                         items: result
                     });
                     console.log(this.state);
                 })
     }

     componentDidMount() {
         this.getComponent()
     }


     showModal = (e) => {
         this.setState({isOpen: true});
         this.setState({id: e.target.id})
     }

     closeModal = (e) => {
         if(e.target.className==='popup'){
             this.setState({isOpen: false});
         }
     }

     closeBtn = () =>{
         this.setState({isOpen: false});
     }

     changeColor = (id) => {
         const {items} = this.state
         console.log(id);

         let favoriteItem = JSON.parse(localStorage.getItem('favorites'))|| [];
         console.log(favoriteItem);
         favoriteItem = (favoriteItem.includes(id)? favoriteItem.filter(i => i !== id):favoriteItem.concat([id]))
         let favoriteItemsArr = JSON.stringify(favoriteItem)
         localStorage. setItem ('favorites', favoriteItemsArr);
         const newItems = items.map(i =>{
             if(i.article === id){
                 i.favorite = !i.favorite
                 console.log(i);
             }
             return i
         })
         this.setState({items: newItems})
     }

     acceptAddFavorite = (e) =>{
         const id = e.target.id
         let acceptItem = JSON.parse(localStorage.getItem('accept'))|| [];
         console.log(acceptItem);
         acceptItem = (acceptItem.includes(id)? acceptItem.filter(i => i !== id):acceptItem.concat([id]))
         let acceptItemsArr = JSON.stringify(acceptItem)
         localStorage. setItem ('accept', acceptItemsArr);
         this.setState({isOpen: false});
     }



      // favoriteAddLocalStorage = (id) =>{
      //     console.log(id);
      //      localStorage. setItem ('article', JSON.stringify(id));
      //
      // }


     render(){
         const {isOpen, items, id} = this.state
         return (
             <div className={'content-block'}>
                 <List/>
                 <Cards
                     item={items} showModal={this.showModal} id={id}
                     color={this.fill} changeColor={this.changeColor}
                     favoriteAddLocalStorage={this.favoriteAddLocalStorage}
                     state = {this.state}
                 />
                 <div onClick={(e)=>this.closeModal(e)}>
                     <Modal
                         open={isOpen} close={isOpen} closeBtn={this.closeBtn}
                         actions={{
                             btn: () => (
                                 <div className={'btn-block'}>
                                     <button className={'btn-ok'} id={this.state.id} onClick={(e) => this.acceptAddFavorite(e)}>OK</button>
                                     <button className={'btn-close'} onClick={this.closeBtn}>Close</button>
                                 </div>
                             )
                         }}
                     />
                 </div>
             </div>
         )
     }
 }

export default App;

