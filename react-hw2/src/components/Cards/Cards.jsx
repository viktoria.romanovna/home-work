import React from 'react'
import className from './Cards.css'
import Svg from "../SVG/Svg";
import PropTypes from 'prop-types';


class Cards extends React.Component {


    render(){
        const {item, showModal, changeColor, state, favoriteAddLocalStorage} = this.props
        return(
            <div className={'cards'}>
                {item.map(elem => {
                   return (
                       <div className={'cards-elem'} key={elem.article}>
                           <p className={'cards-title'}>{elem.name}</p>
                           <img src={`${elem.img}`} width={'1000px'}/>
                           <div className={'def-div'} key={elem.article}>
                               <p>{elem.article}</p>
                               <p>{elem.price}</p>
                           </div>
                           <div className={'def-div'}>
                               <button onClick={(e)=>showModal(e)} id={elem.article} className={'button-drive'}>Online резерв</button>
                               <Svg fill={elem.favorite} changeColor={changeColor} favoriteAddLocalStorage={favoriteAddLocalStorage} state={state} id={elem.article}/>
                           </div>
                       </div>
                   )
                }
                )}
            </div>
        )
    }

}

Cards.propTypes = {
    item: PropTypes.array.isRequired,
    showModal: PropTypes.func,
    changeColor: PropTypes.func,
    state: PropTypes.bool.isRequired,
    favoriteAddLocalStorage: PropTypes.func
}

export default Cards;