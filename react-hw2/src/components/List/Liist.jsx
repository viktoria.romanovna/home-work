import React from 'react'
import className from './Lists.css'
import Cards from "../Cards/Cards";

class List extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            items: []
        };
    }

    componentDidMount() {
        fetch("/cards.json")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        items: result
                    });
                })}

    render(){
        const {items} = this.state
        return(
            <ul className={'lists'}>
                {items.map(item => {
                    return (
                        <li className={'lists-elem'} key={item.article}>{item.name}</li>
                    )
                })}
            </ul>
        )
    }

}

// List.propTypes = {
//     item: PropTypes.array.isRequired,
//     state: PropTypes.bool.isRequired,
// }

export default List;