import React from 'react'
import className from './Modal.css'
import Cards from "../Cards/Cards";

class Modal extends React.Component {

    render() {
        const {open, closeBtn, actions} = this.props
        if (open) {
            return (
                <div className={'popup'}>
                    <div className={'modal'}>
                        <header className={'modal-header'}>
                            <h2 className={'modal-title'}>
                                ONLINE резерв
                            </h2>
                            <button className={'close-x'} onClick={closeBtn}>
                                x
                            </button>
                        </header>
                        <div className={'modal-info'}>
                            <p className={'modal-text'}>Желаете добавить позицию в резерв?</p>
                            {actions.btn()}
                        </div>
                    </div>
                </div>
            )
        }
         return ('')
    }
}

// Modal.propTypes = {
//     open: PropTypes.bool.isRequired,
//     closeBtn: PropTypes.func.isRequired,
//     actions: PropTypes.func.isRequired
// }

export default Modal;